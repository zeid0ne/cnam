package vue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JOptionPane;

import controller.Controller;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import model.Car;

public class Vue extends Application {

	public static Pane pane;
	public VBox layout = new VBox();
	public Stage stage;
	public int w = 1000;
	public int h = 500;
	public Map<Integer, ImageView> carsByCrates = new TreeMap<>();
	public Map<String, ImageView> carsWaiting = new TreeMap<>();
	public int c = 5;
	public Controller controller;

	public static void main(String[] args) {
		Application.launch(Vue.class, args);
	}

	@Override
	public void start(Stage root) throws Exception {
		controller = new Controller(this, c);
		root.setTitle("Vinci - p�age");
		root.setWidth(w);
		root.setHeight(h);
		root.setResizable(false);
		init(root);
		root.show();
	}

	private static void add(Node n) {
		pane.getChildren().add(n);
	}

	private void init(Stage stage) {
		this.stage = stage;
		Group root = new Group();
		Scene scene = new Scene(root);
		root.getChildren().add(layout);
		pane = new Pane();
		buildCrates();
		Label poissonLabel = new Label("Moyenne poisson = " + controller.getLambda());
		poissonLabel.setPrefSize(170, 30);
		poissonLabel.relocate(10, 420);
		add(poissonLabel);
		TextField poissonValue = new TextField();
		poissonValue.setPrefSize(100, 30);
		poissonValue.relocate(170, 420);
		poissonValue.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				controller.changePoissonLambda(poissonValue.getText(), poissonLabel);
			}
		});
		add(poissonValue);
		layout.getChildren().add(pane);
		stage.setScene(scene);
	}

	private void buildCrates() {
		Line line1 = new Line(0, 0, 7 * w / 10, 0);
		add(line1);
		Line line2 = new Line(7 * w / 10, 0, w, (9 * h / 20) - h / 10);
		add(line2);
		for (int i = 0; i < c; i++) {
			Label l = new Label("Crates n� " + i);
			l.setPrefSize(w / 10, h / 10);
			int hcrates = i * ((h - h / 5) / c);
			l.relocate(w / 20, hcrates);
			add(l);
			hcrates = (1 + i) * ((h - h / 5) / c);
			Line line = new Line(0, hcrates, 7 * w / 10, hcrates);
			add(line);
			if (i + 1 == c) {
				Line line3 = new Line(7 * w / 10, hcrates, w, (9 * h / 20) + h / 10);
				add(line3);
			}
		}
	}

	public void addCarToCrates(String numberPlate, int nbCrates, Map<Integer, Car> carsByPlaces) {
		ImageView iv = carsWaiting.get(numberPlate);
		for (ImageView oldIv : carsWaiting.values()) {
			pane.getChildren().remove(oldIv);
		};
		int he = nbCrates * ((h - h / 5) / c);
		iv.relocate(w / 7, he + h / 100);
		add(iv);	
		carsWaiting = new HashMap<>();
		carsByCrates.put(nbCrates, iv);
		for (int nPlace : carsByPlaces.keySet()) {
			Car c = carsByPlaces.get(nPlace);
			addCarToWL(c.getNumberPlate(), nPlace);
		}

	}

	public void addCarToWL(String numberPlate, int nPlace) {
		nPlace--; //cause in model first place is 1 but for this algo the place start with 0
		int randomCarImageNumber = new Random().nextInt(6);
		File f = new File("images/car"+randomCarImageNumber+".jpg");
		Image i = new Image(f.toURI().toString(), w / 10, h / 10 - h / 50, false, true);
		int x = nPlace / 8;
		int y = Math.floorMod(nPlace, 8);
		int wi = x * w / 10 + w / 2;
		int he = y * h / 10;
		ImageView iv = new ImageView(i);
		iv.relocate(wi, he);
		carsWaiting.put(numberPlate, iv);
		add(iv);
	}

	public void removeCar(int nbCrates) {
		ImageView iv = carsByCrates.get(nbCrates);
		pane.getChildren().remove(iv);
	}
	
	private Object lock = new Object();
	public void beautifulMove(ImageView iv, double destX, double destY, int time) {
		double distanceX = Math.abs(iv.getLayoutX()- destX);
		double distanceY = Math.abs(iv.getLayoutY() - destY);
		boolean addX = iv.getLayoutX() < destX;
		boolean addY = iv.getLayoutY() < destY;
		int timerRepetition = 10;
		AtomicInteger count = new AtomicInteger(0);
		Timer t = new Timer();
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				double newX = 0;
				double newY = 0;
				if(addY) {
					newY = iv.getLayoutY() + distanceY;
				} else {
					newY = iv.getLayoutY() - distanceY;
				}
				if(addX) {
					newX = iv.getLayoutX() + distanceX;
				} else {
					newX = iv.getLayoutX() - distanceX;
				}
				iv.relocate(newX, newY);
				JOptionPane.showConfirmDialog(null, "");
				if(count.incrementAndGet() == 10) {
					t.cancel();
					synchronized (lock) {
						lock.notify();	
					}
				}
			}
		};
		t.schedule(tt, time/timerRepetition, time/timerRepetition);	
		try {
			synchronized (lock) {
				lock.wait();	
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


}
