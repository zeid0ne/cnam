package controller;

import javafx.scene.control.Label;
import model.Station;
import vue.Vue;

public class Controller {
	
	private Station station;
	private Vue vue;
	
	public Controller(Vue vue, int c) {
		this.station = new Station(c, this);
		this.vue = vue;
		this.station.start();
	}

	public void changePoissonLambda(String newValue, Label poissonLabel) {
		try {
			long l = Long.valueOf(newValue);
			station.setLambda(l);
			poissonLabel.setText("Moyenne poisson = "+station.getLambda());
		} catch(Exception e ){
			
		}	
	}

	public Long getLambda() {
		return station.getLambda();
	}
	
	public Vue getVue() {
		return vue;
	}

}
