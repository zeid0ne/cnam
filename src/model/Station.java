package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import controller.Controller;
import javafx.application.Platform;


public class Station {

	private long lambda = 10;
	
	private long motorPeriod = 2000;
	
	private int numberPlateLength = 5;
	
	private List<Crates> crates;

	private WaitingLine wl;
	
	private Controller ctrl;

	public Station(int c, Controller ctrl) {
		this.ctrl = ctrl;
		crates = new ArrayList<>();
		for (int i = 0; i < c; i++) {
			crates.add(new Crates(i));
		}
		setWl(new WaitingLine());
	}

	public int getC() {
		return crates.size();
	}

	public WaitingLine getWl() {
		return wl;
	}

	public void setWl(WaitingLine wl) {
		this.wl = wl;
	}

	public int wlSize() {
		return getWl().size();
	}

	private Object lock = new Object(); 
	public void welcome(Car car) {
		if (getWl().hasAPlace()) {
			System.out.println("Car " + car.getNumberPlate() + " enter to the station");
			int n = getWl().add(car);
			Platform.runLater(() -> {
				ctrl.getVue().addCarToWL(car.getNumberPlate(), n);
				synchronized (lock) {
					lock.notify();	
				}
			});
			try {
				synchronized (lock) {
					lock.wait();	
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void free(Crates crates) {
		crates.free(ctrl.getVue());
	}

	public void start() {
		Timer t = new Timer();
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				System.out.println();
				System.out.println("//////MOTOR PERIOD//////");
				System.out.println("Waiting File Length ["+wlSize()+"]");
				System.out.println();
				// arrivee des voitures
				for (int i = 0; i < poisson(lambda); i++) {
					welcome(new Car(randomString(numberPlateLength)));
				}
				for (Crates cr : crates) {
					// paiement des caisses
					cr.free(ctrl.getVue());
					if (cr.isFree()) {
						// envoie des voitures dans les caisses
						wl.sendNext(cr, ctrl.getVue());
					}
				}
			}
		};
		t.schedule(tt, motorPeriod, motorPeriod);		
	}
	
	  private int poisson( double lambda ) {
	      double elambda = Math.exp(-1*lambda);
	      double product = 1;
	      int count =  0;
	      do {
	        product *= new Random().nextDouble();
	        count++ ;
	      } while( product >= elambda ) ;
	      return count;
	    }

	public String randomString(int length) {
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";																					// dont tu ne veux pas
		String pass = "";
		for (int x = 0; x < length; x++) {
			int i = (int) Math.floor(Math.random() * 62);
			pass += chars.charAt(i);
		}
		return pass;
	}
	
	public long getLambda() {
		return this.lambda;
	}
	
	public void setLambda(long value) {
		this.lambda = value;
	}

}
