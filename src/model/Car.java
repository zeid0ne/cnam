package model;

import java.util.Random;

public class Car {
	
	private String numberPlate;
	
	private long timeToPay;
	
	public Car(String numberPlate) {
		this.numberPlate = numberPlate;
		timeToPay = 1000*(new Random().nextInt(5)+1);
	}
	
	public String getNumberPlate() {
		return numberPlate;
	}
	
	public void comeToStation(Station station) {
		station.welcome(this);
	}

	public long timeToPay() {
		return timeToPay;
	}
}
