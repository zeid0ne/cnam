package model;

import java.util.HashMap;
import java.util.Map;

import javafx.application.Platform;
import vue.Vue;

public class WaitingLine {
	
	//TODO the cars in the waiting line forward after free of the cars in crates

	public int wlPlaces = 35;
	private Map<Integer,Car> carsByPlaces;

	public WaitingLine() {
		carsByPlaces = new HashMap<>();
	}

	public int add(Car car) {
		int n = nextFreePlace();
		carsByPlaces.put(n,car);
		return n;
	}

	private int nextFreePlace() {
		for (int i = 1; i <= wlPlaces; i++) {
			if (carsByPlaces.containsKey(i)) {
				continue;
			}
			return i;
		}
		return -1;
	}
	
	private int nextOccupedPlace() {
		for (int i = 1; i <= wlPlaces; i++) {
			if (!carsByPlaces.containsKey(i)) {
				continue;
			}
			return i;
		}
		return -1;
	}

	public int size() {
		return carsByPlaces.size();
	}

	public boolean isEmpty() {
		return carsByPlaces.size() == 0;
	}

	private Object lock = new Object(); 
	public void sendNext(Crates c, Vue v) {
		if (!isEmpty()) {
			int place = nextOccupedPlace();
			Car car = carsByPlaces.get(place);
			c.welcome(car);
			carsByPlaces.remove(place);
			System.out.println("Car " + car.getNumberPlate() + " is sent to crates " + c.getNumber());
			Map<Integer,Car> n = new HashMap<>();
			for (int nPlace : carsByPlaces.keySet()) {
				n.put(nPlace-1, carsByPlaces.get(nPlace));
			}
			carsByPlaces = n;
			Platform.runLater(() -> {
				v.addCarToCrates(car.getNumberPlate(), c.getNumber(), carsByPlaces);	
				synchronized (lock) {
					lock.notify();	
				}
			});
			try {
				synchronized (lock) {
					lock.wait();	
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public int getWlPLaces() {
		return this.wlPlaces;
	}
	
	public boolean hasAPlace() {
		return size() < getWlPLaces();
	}

}
