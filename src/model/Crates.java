package model;

import javafx.application.Platform;
import vue.Vue;

public class Crates {

	private int number;

	private Car car;

	private long comingTime;

	public Crates(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public boolean isFree() {
		return car == null;
	}

	/**
	 * If car end to pay, let it go
	 */
	private Object lock = new Object(); 
	public void free(Vue vue) {
		if (!isFree() && (System.currentTimeMillis() - comingTime) >= car.timeToPay()) {
			System.out.println("Car " + car.getNumberPlate() + " paid and go out of the station");
			car = null;
			Platform.runLater(()->{
				vue.removeCar(getNumber());
				synchronized (lock) {
					lock.notify();	
				}
			});
			try {
				synchronized (lock) {
					lock.wait();	
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void welcome(Car car) {
		this.car = car;
		comingTime = System.currentTimeMillis();
	}

}
